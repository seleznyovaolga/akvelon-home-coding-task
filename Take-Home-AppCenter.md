**Akvelon Take Home coding task (AppCenter)**
1. Create any app in https://appcenter.ms, then configure several branches.
2. Implement C# Console App or NodeJS console app (or any other preferred app type/language) which does the following using App Center API(https://openapi.appcenter.ms/#/build)
- Receive list of branches for the app and build them
- Print the following information to console output
    1. < branch name > build < completed/failed > in 125 seconds. Link to build logs: < link >
    2.
3. Publish code to the GitHub repo and write Readme.md with description and usage instructions (in English).

**We really appreciate your time working on this task so would like to add extra clarity of how this task will help us to evaluate you.**
1. Ability to learn new things/tools - understand what is AppCenter / how it works / configure new app.
2. Research and troubleshooting skills - understand App Center API and implement simple integration
3. Coding skills - write human readable/maintanable code, perform API calls
4. Tips
- Feel free to ask clarification questions (unclear points, selecting between implementation alternatives, etc)
- Ensure code quality - treat this task as a real project other engineers will be working with
- Use inline comments/notes (or Readme.md) to point to everything which could be done/improved (no need to implement everything in ideal manner; we understand that you are busy and appreciate your time) 
