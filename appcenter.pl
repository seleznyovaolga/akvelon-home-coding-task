#!/usr/bin/perl
use JSON::XS;
use Date::Parse;
use Data::Dumper;


$app_name="BloodBank";
$api_token = &promptUser("Enter API token");
$owner_name = &promptUser("Enter owner name");


#connect_to_repo();

#Connect GitLab repo to app
sub connect_to_repo
{
	if(`curl -sL -w "%{http_code}" -X POST -H  "accept: application/json" -H  "X-API-Token: $api_token" -H  "Content-Type: application/json" -d '{  "repo_url": "https://gitlab.com/home383/BloodBank.git",  "repo_id" : "36890618",  "external_user_id" : "9301380"}' "https://api.appcenter.ms/v0.1/apps/$owner_name/$app_name/repo_config" -o /dev/null`=~ /200/)
	{
		print("Repository https://gitlab.com/home383/BloodBank.git is connected to $app_name app\n");
	}
	else
	{
		print("Error connecting repo to $app_name app\n");
		exit 1; 
	}
}

#Retrieve branch list for the repo 
print "Retrieving branches for $app_name";
my $result=`curl -X GET "https://api.appcenter.ms/v0.1/apps/$owner_name/$app_name/branches" -H  "accept: application/json" -H  "X-API-Token: $api_token"`;
my $builds = decode_json $result;
print "Cheacking presence of build configs for $app_name all branches\n";
for my $branch (@$builds ){
	my $branch_name=$branch->{branch}->{name};
	my $last_commit=$branch->{branch}->{commit}->{sha};
	#Create build config for all the branches 

	if(`curl -sL -w "%{http_code}" -X GET "https://api.appcenter.ms/v0.1/apps/$owner_name/$app_name/branches/$branch_name/config" -H  "accept: application/json" -H  "X-API-Token: $api_token" -o /dev/null`=~ /200/)
	{
		print("Build config already created for branch $branch_name $app_name app.\n");
	}
	else
	{
		print("Creating build config fo $branch_name $app_name app.\n");
		if(`curl -sL -w "%{http_code}" -X POST "https://api.appcenter.ms/v0.1/apps/$owner_name/$app_name/branches/$branch_name/config" -H  "accept: application/json" -H  "X-API-Token: d154732cdffd11e0a52a683df6689795cdefe128" -H  "Content-Type: application/json" -d '{  "trigger": "continous",  "testsEnabled": true,  "badgeIsEnabled": true,  "signed": true,  "cloneFromBranch": "2.1",  "toolsets": {    "xcode": {      "projectOrWorkspacePath": "Example.xcodeproj/project.xcworkspace",      "podfilePath": "string",      "cartfilePath": "Cartfile.resolved",      "provisioningProfileEncoded": "string",      "certificateEncoded": "string",      "provisioningProfileFileId": "string",      "certificateFileId": "string",      "provisioningProfileUploadId": "string",      "appExtensionProvisioningProfileFiles": [        {          "fileName": "string",          "fileId": "string",          "uploadId": "string",          "targetBundleIdentifier": "string"        }      ],      "certificateUploadId": "string",      "certificatePassword": "string",      "scheme": "Example",      "xcodeVersion": "13.3.1",      "provisioningProfileFilename": "string",      "certificateFilename": "string",      "teamId": "string",      "automaticSigning": true,      "xcodeProjectSha": "string",      "archiveConfiguration": "string",      "targetToArchive": "string",      "forceLegacyBuildSystem": true    },    "javascript": {      "packageJsonPath": "package.json",      "runTests": true,      "reactNativeVersion": "string"    },    "xamarin": {      "slnPath": "string",      "isSimBuild": true,      "args": "string",      "configuration": "string",      "p12File": "string",      "p12Pwd": "string",      "provProfile": "string",      "monoVersion": "string",      "sdkBundle": "string",      "symlink": "string"    },    "android": {      "gradleWrapperPath": "android/gradlew",      "module": "app",      "buildVariant": "release",      "runTests": true,      "runLint": true,      "isRoot": true,      "automaticSigning": true,      "keystorePassword": "string",      "keyAlias": "string",      "keyPassword": "string",      "keystoreFilename": "string",      "keystoreEncoded": "string"    }  },  "artifactVersioning": {    "buildNumberFormat": "buildId"  },  "additionalProp1": {    "branch": {      "name": "string",      "commit": {        "sha": "98fb3a01826f4292e6c52f466105e1ac16ff18b3",        "url": "string"      }    },    "enabled": true  },  "additionalProp2": {    "branch": {      "name": "string",      "commit": {        "sha": "string",        "url": "string"      }    },    "enabled": true  },  "additionalProp3": {    "branch": {      "name": "string",      "commit": {        "sha": "string",        "url": "string"      }    },    "enabled": true  }}'  -o /dev/null`=~ /200/)
		{
        		print("Build config created for branch $branch_name $app_name app\n");
		}
		else
		{
        		print("There was error creating build config for $branch_name $app_name app.\n");
		}
	}
	#Run build for the last commit of all the branches
	print "Running build for the last commit of the branch $branch_name\n";
	$result=`curl -X POST "https://api.appcenter.ms/v0.1/apps/$owner_name/$app_name/branches/$branch_name/builds" -H  "accept: application/json" -H  "X-API-Token: $api_token" -H  "Content-Type: application/json" -d '{  "sourceVersion": "$last_commit",  "debug": true}'`;
	my $build_number = decode_json $result;
	my $build_number_id=$build_number->{id};
	#Get the status of all the builds
	CHECK_STATUS:sleep(10);
	$result=`curl -X GET "https://api.appcenter.ms/v0.1/apps/$owner_name/$app_name/builds/$build_number_id" -H  "accept: application/json" -H  "X-API-Token: $api_token"`;
	my $build_result = decode_json $result;
	my $build_status=$build_result->{status};
	if($build_status =~ "completed")
	{	
		my $duration=str2time($build_result->{finishTime})-str2time($build_result->{startTime});
		print "$branch_name build $build_status in  $duration seconds. Link to build logs: https://appcenter.ms/users/$owner_name/apps/$app_name/build/branches/$branch_name/builds/$build_number_id\n";
	}
	else
	{
		print "Still building...";
		goto CHECK_STATUS;
	}
}

sub promptUser {
   local($promptString,$defaultValue) = @_;
   if ($defaultValue) {
      print $promptString, "[", $defaultValue, "]: ";
   } else {
      print $promptString, ": ";
   }
   $| = 1;
   $_ = <STDIN>;
   chomp;

   if ("$defaultValue") {
      return $_ ? $_ : $defaultValue;    # return $_ if it has a value
   } else {
      return $_;
   }
}
